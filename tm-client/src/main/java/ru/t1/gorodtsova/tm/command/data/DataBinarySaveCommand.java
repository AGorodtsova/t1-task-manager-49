package ru.t1.gorodtsova.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.dto.request.domain.DataBinarySaveRequest;

public final class DataBinarySaveCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Save data to binary file";

    @NotNull
    private final String NAME = "data-save-bin";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[DATA SAVE BINARY]");
        @NotNull final DataBinarySaveRequest request = new DataBinarySaveRequest(getToken());
        getDomainEndpoint().saveDataBinary(request);
    }

}
