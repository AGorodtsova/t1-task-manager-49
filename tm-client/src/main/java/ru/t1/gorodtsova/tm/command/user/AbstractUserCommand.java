package ru.t1.gorodtsova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.gorodtsova.tm.api.endpoint.IUserEndpoint;
import ru.t1.gorodtsova.tm.command.AbstractCommand;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected IUserEndpoint getUserEndpoint() {
        return getServiceLocator().getUserEndpoint();
    }

    @NotNull
    protected IAuthEndpoint getAuthEndpoint() {
        return getServiceLocator().getAuthEndpoint();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
