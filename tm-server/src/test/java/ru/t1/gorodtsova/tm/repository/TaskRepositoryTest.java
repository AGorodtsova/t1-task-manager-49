package ru.t1.gorodtsova.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.gorodtsova.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.gorodtsova.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.gorodtsova.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.gorodtsova.tm.api.service.IConnectionService;
import ru.t1.gorodtsova.tm.api.service.IPropertyService;
import ru.t1.gorodtsova.tm.marker.UnitCategory;
import ru.t1.gorodtsova.tm.repository.dto.ProjectDtoRepository;
import ru.t1.gorodtsova.tm.repository.dto.TaskDtoRepository;
import ru.t1.gorodtsova.tm.repository.dto.UserDtoRepository;
import ru.t1.gorodtsova.tm.service.ConnectionService;
import ru.t1.gorodtsova.tm.service.PropertyService;

import javax.persistence.EntityManager;

import static ru.t1.gorodtsova.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.t1.gorodtsova.tm.constant.ProjectTestData.USER2_PROJECT1;
import static ru.t1.gorodtsova.tm.constant.TaskTestData.*;
import static ru.t1.gorodtsova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    private ITaskDtoRepository getRepository(@NotNull final EntityManager entityManager) {
        return new TaskDtoRepository(entityManager);
    }

    @Before
    @SneakyThrows
    public void init() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            userRepository.add(USER1);
            userRepository.add(USER2);
            userRepository.add(ADMIN1);
            entityManager.getTransaction().commit();

            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.add(USER1_PROJECT1);
            projectRepository.add(USER2_PROJECT1);
            entityManager.getTransaction().commit();

        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @After
    @SneakyThrows
    public void tearDown() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDtoRepository userRepository = new UserDtoRepository(entityManager);
            @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
            @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.removeAll();
            projectRepository.removeAll();
            userRepository.removeAll();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void add() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_TASK1);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER1_TASK1, repository.findAll().get(0));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void addByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1.getId(), USER1_TASK1);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER1_TASK1, repository.findAll().get(0));
            Assert.assertEquals(USER1.getId(), repository.findAll().get(0).getUserId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void addAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_TASK_LIST);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER1_TASK_LIST, repository.findAll());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findAllByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(TASK_LIST);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER1_TASK_LIST, repository.findAll(USER1.getId()));
            Assert.assertNotEquals(USER1_TASK_LIST, repository.findAll(USER2.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findOneByIdByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_TASK1);
            repository.add(USER2_TASK1);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER1_TASK1, repository.findOneById(USER1.getId(), USER1_TASK1.getId()));
            Assert.assertNotEquals(USER2_TASK1, repository.findOneById(USER1.getId(), USER2_TASK1.getId()));
            Assert.assertNull(repository.findOneById(USER1.getId(), USER2_TASK1.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void removeByIdByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_TASK1);
            repository.add(USER2_TASK1);
            repository.removeOneById(USER1.getId(), USER1_TASK1.getId());
            entityManager.getTransaction().commit();
            Assert.assertFalse(repository.findAll().contains(USER1_TASK1));
            Assert.assertTrue(repository.findAll().contains(USER2_TASK1));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void removeByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_TASK1);
            repository.add(USER2_TASK1);
            repository.removeOne(USER1.getId(), USER1_TASK1);
            entityManager.getTransaction().commit();
            Assert.assertFalse(repository.findAll().contains(USER1_TASK1));
            Assert.assertTrue(repository.findAll().contains(USER2_TASK1));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void removeAllByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_TASK_LIST);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER1_TASK_LIST, repository.findAll());

            entityManager.getTransaction().begin();
            repository.removeAll(USER1.getId());
            entityManager.getTransaction().commit();
            Assert.assertTrue(repository.findAll().isEmpty());

            entityManager.getTransaction().begin();
            repository.add(USER2_TASK1);
            repository.removeAll(USER1.getId());
            Assert.assertFalse(repository.findAll().isEmpty());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void existsByIdByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_TASK1);
            entityManager.getTransaction().commit();
            Assert.assertTrue(repository.existsById(USER1_TASK1.getId()));
            Assert.assertFalse(repository.existsById(USER2_TASK1.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void findAllByProjectId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskDtoRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_TASK_LIST);
            repository.add(USER2_TASK_LIST);
            entityManager.getTransaction().commit();
            Assert.assertEquals(USER1_TASK_LIST, repository.findAllByProjectId(USER1.getId(), USER1_PROJECT1.getId()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
