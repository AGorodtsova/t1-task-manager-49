package ru.t1.gorodtsova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.gorodtsova.tm.api.service.IConnectionService;
import ru.t1.gorodtsova.tm.api.service.IPropertyService;
import ru.t1.gorodtsova.tm.api.service.dto.IUserDtoService;
import ru.t1.gorodtsova.tm.dto.model.UserDTO;
import ru.t1.gorodtsova.tm.enumerated.Role;
import ru.t1.gorodtsova.tm.exception.entity.UserNotFoundException;
import ru.t1.gorodtsova.tm.exception.field.EmailEmptyException;
import ru.t1.gorodtsova.tm.exception.field.IdEmptyException;
import ru.t1.gorodtsova.tm.exception.field.LoginEmptyException;
import ru.t1.gorodtsova.tm.exception.field.PasswordEmptyException;
import ru.t1.gorodtsova.tm.exception.user.ExistsLoginException;
import ru.t1.gorodtsova.tm.marker.UnitCategory;
import ru.t1.gorodtsova.tm.service.dto.UserDtoService;
import ru.t1.gorodtsova.tm.util.HashUtil;

import javax.persistence.EntityNotFoundException;

import static ru.t1.gorodtsova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserServiceTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IUserDtoService userService = new UserDtoService(connectionService, propertyService);

    @Rule
    @NotNull
    public final ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup() {
        userService.add(USER_LIST);
    }

    @After
    public void tearDown() {
        userService.removeAll();
    }

    @Test
    public void add() {
        userService.removeAll();
        userService.add(USER1);
        Assert.assertEquals(USER1.getId(), userService.findAll().get(0).getId());
    }

    @Test
    public void addAll() {
        userService.removeAll();
        userService.add(USER_LIST1);
        Assert.assertEquals(USER_LIST1.size(), userService.findAll().size());

    }

    @Test
    public void set() {
        Assert.assertFalse(userService.findAll().isEmpty());
        userService.set(USER_LIST1);
        Assert.assertEquals(USER_LIST1.size(), userService.findAll().size());
        Assert.assertNotEquals(USER_LIST2.size(), userService.findAll().size());
    }

    @Test
    public void findAll() {
        Assert.assertEquals(USER_LIST.size(), userService.findAll().size());
    }

    @Test
    public void findOneById() {
        Assert.assertEquals(USER1.getId(), userService.findOneById(USER1.getId()).getId());
        Assert.assertNotEquals(USER1.getId(), userService.findOneById(USER2.getId()).getId());

        thrown.expect(EntityNotFoundException.class);
        @NotNull final UserDTO user = new UserDTO();
        userService.findOneById(user.getId());

        thrown.expect(IdEmptyException.class);
        userService.findOneById(null);
    }

    @Test
    public void removeAll() {
        Assert.assertFalse(userService.findAll().isEmpty());
        userService.removeAll();
        Assert.assertTrue(userService.findAll().isEmpty());
    }

    @Test
    public void removeAllCollection() {
        Assert.assertEquals(USER_LIST.size(), userService.findAll().size());
        userService.removeAll(USER_LIST1);
        Assert.assertFalse(userService.findAll().isEmpty());
        Assert.assertEquals(USER_LIST2.size(), userService.findAll().size());
    }

    @Test
    public void removeOneById() {
        userService.removeOneById(USER1.getId());
        Assert.assertFalse(userService.existsById(USER1.getId()));

        thrown.expect(EntityNotFoundException.class);
        userService.findOneById(USER1.getId());

        thrown.expect(IdEmptyException.class);
        userService.removeOneById(null);
    }

    @Test
    public void existsById() {
        Assert.assertTrue(userService.existsById(USER1.getId()));
        @NotNull final UserDTO user = new UserDTO();
        Assert.assertFalse(userService.existsById(user.getId()));
        Assert.assertFalse(userService.existsById(null));
    }

    @Test
    public void createUserLoginPass() {
        @NotNull final UserDTO user = userService.create("testuser", "testpass");
        Assert.assertEquals(user.getId(), userService.findOneById(user.getId()).getId());
        Assert.assertEquals("testuser", user.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, "testpass"), user.getPasswordHash());

        thrown.expect(LoginEmptyException.class);
        userService.create(null, USER1.getPasswordHash());

        thrown.expect(ExistsLoginException.class);
        userService.create(USER1.getLogin(), USER1.getPasswordHash());

        thrown.expect(PasswordEmptyException.class);
        userService.create("login", null);
    }

    @Test
    public void createUserLoginPassEmail() {
        @NotNull UserDTO user = userService.create("testuser", "testpass", "user@company.ru");
        Assert.assertTrue(userService.existsById(user.getId()));
        Assert.assertEquals("testuser", user.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, "testpass"), user.getPasswordHash());
        Assert.assertEquals("user@company.ru", user.getEmail());
    }

    @Test
    public void createUserLoginPassRole() {
        @NotNull UserDTO user = userService.create("testuser", "testpass", Role.USUAL);
        Assert.assertTrue(userService.existsById(user.getId()));
        Assert.assertEquals("testuser", user.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, "testpass"), user.getPasswordHash());
        Assert.assertEquals(Role.USUAL, user.getRole());
    }

    @Test
    public void updateUser() {
        userService.updateUser(USER1.getId(), "new first name", "new last name", "new middle name");
        Assert.assertEquals("new first name", userService.findOneById(USER1.getId()).getFirstName());
        Assert.assertEquals("new last name", userService.findOneById(USER1.getId()).getLastName());
        Assert.assertEquals("new middle name", userService.findOneById(USER1.getId()).getMiddleName());

        thrown.expect(IdEmptyException.class);
        userService.updateUser(null, "new first name", "new last name", "new description");

        @NotNull final UserDTO user = new UserDTO();
        thrown.expect(EntityNotFoundException.class);
        userService.updateUser(user.getId(), "new first name", "new last name", "new middle name");
    }

    @Test
    public void findByLogin() {
        Assert.assertEquals(USER1.getId(), userService.findByLogin(USER1.getLogin()).getId());
        Assert.assertNotEquals(USER1.getId(), userService.findByLogin(USER2.getLogin()).getId());

        thrown.expect(LoginEmptyException.class);
        userService.findByLogin(null);

        @NotNull final UserDTO user = new UserDTO();
        user.setLogin("login");
        thrown.expect(UserNotFoundException.class);
        userService.findByLogin(user.getLogin());
    }

    @Test
    public void findByEmail() {
        Assert.assertEquals(USER1.getId(), userService.findByEmail(USER1.getEmail()).getId());
        Assert.assertNotEquals(USER1.getId(), userService.findByEmail(USER2.getEmail()).getId());

        thrown.expect(EmailEmptyException.class);
        userService.findByEmail(null);

        @NotNull final UserDTO user = new UserDTO();
        user.setEmail("user@company.ru");
        thrown.expect(UserNotFoundException.class);
        userService.findByEmail(user.getEmail());
    }

    @Test
    public void isLoginExist() {
        Assert.assertTrue(userService.isLoginExist(USER1.getLogin()));
        Assert.assertFalse(userService.isLoginExist("NOT_EXIST_LOGIN"));
        Assert.assertFalse(userService.isLoginExist(""));
    }

    @Test
    public void isEmailExist() {
        Assert.assertTrue(userService.isEmailExist(USER1.getEmail()));
        Assert.assertFalse(userService.isEmailExist("NOT_EXIST_EMAIL"));
        Assert.assertFalse(userService.isEmailExist(""));
    }

    @Test
    public void setPassword() {
        userService.setPassword(USER1.getId(), "new_password");
        Assert.assertEquals(
                HashUtil.salt(propertyService, "new_password"),
                userService.findOneById(USER1.getId()).getPasswordHash()
        );

        thrown.expect(IdEmptyException.class);
        userService.setPassword(null, "new_password");

        thrown.expect(PasswordEmptyException.class);
        userService.setPassword(USER1.getId(), "");

        @NotNull final UserDTO user = new UserDTO();
        user.setLogin("login");
        user.setPasswordHash("password");
        thrown.expect(UserNotFoundException.class);
        userService.setPassword(user.getId(), "new_password");

    }

    @Test
    public void lockUserByLogin() {
        Assert.assertFalse(userService.findOneById(USER1.getId()).getLocked());
        userService.lockUserByLogin(USER1.getLogin());
        Assert.assertTrue(userService.findOneById(USER1.getId()).getLocked());

        @NotNull final UserDTO user = new UserDTO();
        thrown.expect(LoginEmptyException.class);
        userService.lockUserByLogin(user.getLogin());
    }

    @Test
    public void unlockUserByLogin() {
        userService.lockUserByLogin(USER1.getLogin());
        Assert.assertTrue(userService.findOneById(USER1.getId()).getLocked());
        userService.unlockUserByLogin(USER1.getLogin());
        Assert.assertFalse(userService.findOneById(USER1.getId()).getLocked());

        @NotNull final UserDTO user = new UserDTO();
        thrown.expect(LoginEmptyException.class);
        userService.unlockUserByLogin(user.getLogin());
    }

}
