package ru.t1.gorodtsova.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.api.endpoint.*;
import ru.t1.gorodtsova.tm.api.service.*;
import ru.t1.gorodtsova.tm.api.service.dto.*;
import ru.t1.gorodtsova.tm.endpoint.*;
import ru.t1.gorodtsova.tm.service.*;
import ru.t1.gorodtsova.tm.service.dto.*;
import ru.t1.gorodtsova.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final ITaskDtoService taskService = new TaskDtoService(connectionService);

    @Getter
    @NotNull
    private final IProjectTaskDtoService projectTaskService = new ProjectTaskDtoService(connectionService);

    @Getter
    @NotNull
    private final IProjectDtoService projectService = new ProjectDtoService(connectionService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService(connectionService);

    @Getter
    @NotNull
    private final IUserDtoService userService = new UserDtoService(connectionService, propertyService);

    @Getter
    @NotNull
    private final ISessionDtoService sessionService = new SessionDtoService(connectionService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService, sessionService);

    @Getter
    @NotNull
    public final IDomainService domainService = new DomainService(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK_MANAGER IS SHUTTING DOWN **");
        //backup.stop();
    }

    public void start() {
        initPID();
        loggerService.info("** WELCOME TO TASK MANAGER **");
        loggerService.startJmsLogger();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        //backup.start();
    }

}
